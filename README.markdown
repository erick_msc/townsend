1. Project uses Maven for dependency management.
   pom.xml file could be used to download all required jars

2. This is a testng project.
   start_here.xml is a modified testng xml that allows for multi-browser support as well as changing the url endpoint
   
3. This is a selenium project. Usual selenium setup applies:
   - IEDriver and ChromeDriver should be in path if we want to run IE and Chrome test
   - The selenium version 2.53.1 that comes with this project works with Firefox version 47.0.1
     If later versions of firefox is used, Selenium version will need to be upgraded in pom.xml. Otherwise, Firefox need to be downgraded
