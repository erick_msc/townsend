package townsend.website.one.pages;

import org.openqa.selenium.WebDriver;

public class CreatePage extends DetailsPage {

    /**************************************************************************************
     * Constructor
     **************************************************************************************/
	public CreatePage(WebDriver driver) {
		super(driver);
	}

    /**************************************************************************************
     * Click add button
     **************************************************************************************/
	public void confirmCreate() {
		driver.findElement(actionButton("Add")).click();
	}

}
