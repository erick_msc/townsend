package townsend.website.one.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ListingPage extends PageTemplate {

    /*************************************************************************************
     * Object Repository
     *************************************************************************************/
	protected By actionButton(String buttonLabel) {
		return By.xpath(".//a[normalize-space()=\"" + buttonLabel + "\"]");
	}
	protected By employeeListing = By.cssSelector("ul#employee-list");
		protected By employeeRow(String name) {
			return By.xpath(".//li[normalize-space()=\"" + name + "\"]");
		}

    /**************************************************************************************
     * Constructor
     **************************************************************************************/
	public ListingPage(WebDriver driver) {
		super(driver);
	}
	
	public boolean doesButtonExist(String text) {
		return isElementDisplayed(driver,actionButton(text));
	}

    /**************************************************************************************
     * Click on Create button
     **************************************************************************************/
	public void createNewEmployee() {
		driver.findElement(actionButton("Create")).click();
	}

    /**************************************************************************************
     * Select any item on the list
     **************************************************************************************/
	public void selectEmployeeRow(String name) {

		WebElement fullList = driver.findElement(employeeListing);
		fullList.findElement(employeeRow(name)).click();

	}
	
	public boolean employeeExist(String name) {

		WebElement fullList = driver.findElement(employeeListing);
		return isElementDisplayed(fullList, employeeRow(name));
		
	}

    /**************************************************************************************
     * Select any item on the list and then click Edit
     **************************************************************************************/
	public void editEmployee(String name) {

	    // Select an employee row and click Edit button
		selectEmployeeRow(name);
		driver.findElement(actionButton("Edit")).click();

	}

    /**************************************************************************************
     * Select any item on the list and then click Delete and Ok
     **************************************************************************************/
	public void deleteEmployee(String name) {

	    // Click Delete button
		selectEmployeeRow(name);
		driver.findElement(actionButton("Delete")).click();

	    // Wait 10 seconds till alert is present
	    WebDriverWait wait = new WebDriverWait(driver, 10);
	    Alert alert = wait.until(ExpectedConditions.alertIsPresent());

 	    // Accepting alert.
 	    alert.accept();

	}

}
