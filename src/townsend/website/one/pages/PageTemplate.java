package townsend.website.one.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class PageTemplate {
	
	WebDriver driver;

    /**************************************************************************************
     * Constructor
     **************************************************************************************/
	public PageTemplate(WebDriver driver) {
		this.driver = driver;
	}

    /**************************************************************************************
     * Combines clear and send keys
     * @input - input element or textarea element that allows text entry
     * @newText - text to enter into the input element
     **************************************************************************************/
	public void replaceText(WebElement input, String newText) {
		input.clear();
		input.sendKeys(newText);
	}

    /**************************************************************************************
     * Check if a certain WebElement is present on page and is visible
     * @outer - an area to check, can be of type WebDriver or WebElement
     * @inner - locator to find element by
     **************************************************************************************/
    public boolean isElementDisplayed(WebElement outer, By inner) {
		List<WebElement> a = outer.findElements(inner);
		return a.size() > 0 && a.get(0).isDisplayed();
    }
    public boolean isElementDisplayed(WebDriver outer,By inner) {
		List<WebElement> a = outer.findElements(inner);
		return a.size() > 0 && a.get(0).isDisplayed();
    }

    /**************************************************************************************
     * Wait for x seconds
     **************************************************************************************/
	public void waitFor(double seconds) {
        try {
            Thread.sleep((int) (seconds * 1000.0));                 //1000 milliseconds is one second.
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
	}
}
