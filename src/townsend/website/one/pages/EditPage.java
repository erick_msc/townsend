package townsend.website.one.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditPage extends DetailsPage {

    /*************************************************************************************
     * Object Repository
     *************************************************************************************/
	protected By deleteButton = By.xpath(".//*[contains(concat(' ',@class,' '),\" main-button \") and normalize-space()=\"Delete\"]");

    /**************************************************************************************
     * Constructor
     **************************************************************************************/
	public EditPage(WebDriver driver) {
		super(driver);
	}

    /**************************************************************************************
     * Click delete button
     **************************************************************************************/
	public void deleteObject() {

	    // Click Delete button
		driver.findElement(deleteButton).click();

	    // Wait 10 seconds till alert is present
	    WebDriverWait wait = new WebDriverWait(driver, 10);
	    Alert alert = wait.until(ExpectedConditions.alertIsPresent());

 	    // Accepting alert.
 	    alert.accept();
	}

    /**************************************************************************************
     * Click update button
     **************************************************************************************/
	public void confirmUpdate() {

	    // Click Update button
		driver.findElement(actionButton("Update")).click();

	}
}
