package townsend.website.one.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends PageTemplate {

    /*************************************************************************************
     * Object Repository
     *************************************************************************************/
	protected By loginForm = By.id("login-form");
		protected By errorArea = By.className("error-message");
		protected By entryField(String visibleLabel) {
			return By.xpath(".//span[normalize-space()=\"" + visibleLabel + "\"]/following-sibling::input");
		}
		protected By loginButton = By.xpath(".//button[normalize-space()=\"Login\"]");

    /**************************************************************************************
     * Constructor
     **************************************************************************************/
	public LoginPage(WebDriver driver) {
		super(driver);
	}

    /**************************************************************************************
     * Get the entire login form
     **************************************************************************************/
	public WebElement getLoginForm() {
		return driver.findElement(loginForm);
	}

    /**************************************************************************************
     * Function to login
     * @username - Login username
     * @password - Login password
     **************************************************************************************/
	public void login(String in_username, String in_password) {
		
		// Narrow down the search area specifically to the loginForm
		WebElement myLoginForm = getLoginForm();
		
		// Then perform all the actions within the login form
		replaceText(myLoginForm.findElement(entryField("Username*")),in_username);
		replaceText(myLoginForm.findElement(entryField("Password*")),in_password);
		myLoginForm.findElement(loginButton).click();
		waitFor(1);
	}

    /**************************************************************************************
     * Check if the error field is currently visible to the user
     **************************************************************************************/
	public boolean isErrorVisible() {
		return isElementDisplayed(getLoginForm(),errorArea);
	}

    /**************************************************************************************
     * Get the currently-displayed login message
     **************************************************************************************/
	public String getErrorMessage() {
		return getLoginForm().findElement(errorArea).getText();
	}
}
