package townsend.website.one.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class DetailsPage extends PageTemplate {

    /*************************************************************************************
     * Object Repository
     *************************************************************************************/
	protected By backButton = By.xpath(".//a[normalize-space()=\"Back\"]");
	protected By employeeForm = By.name("employeeForm");
		protected By entryField(String label) {
			return By.xpath(".//label[child::span[normalize-space()=\"" + label + "\"]]/input");
		}
	protected By actionButton(String buttonLabel) {
		return By.xpath(".//button[normalize-space()=\"" + buttonLabel + "\"]");
	}

    /**************************************************************************************
     * Constructor
     **************************************************************************************/
	public DetailsPage(WebDriver driver) {
		super(driver);
	}

    /**************************************************************************************
     * Replace the text on any of the 4 input elements
     * @fieldLabel - The label attached to the input element
     * @newFieldValue - The new value that we want to enter into the field
     **************************************************************************************/
	public void replaceSingleDetail(String fieldLabel, String newFieldValue) {
		WebElement formArea = driver.findElement(employeeForm);
		replaceText(formArea.findElement(entryField(fieldLabel)),newFieldValue);
	}

    /**************************************************************************************
     * Fill out the whole form
     * @firstName - new value for First name field
     * @lastName - new value for Last name field
     * @startDate - new value for Start date field
     * @email - new value for Email field
     **************************************************************************************/
	public void fillInForm(String firstName, String lastName, String startDate, String email) {
		WebElement formArea = driver.findElement(employeeForm);
		replaceText(formArea.findElement(entryField("First name:")), firstName);
		replaceText(formArea.findElement(entryField("Last name:")), lastName);
		replaceText(formArea.findElement(entryField("Start date:")), startDate);
		replaceText(formArea.findElement(entryField("Email:")), email);
	}

    /**************************************************************************************
     * Check if the field is currently in valid state
     * @fieldName - visible label for the particular field
     **************************************************************************************/
	public boolean isFieldValid(String fieldName) {
		WebElement formArea = driver.findElement(employeeForm);
		return (" " + formArea.findElement(entryField(fieldName)).getAttribute("class") + " ").contains(" ng-valid ");
	}

    /**************************************************************************************
     * Get the current value of any field for validation
     * @fieldName - visible label for the particular field
     **************************************************************************************/
	public String getCurrentValue(String fieldName) {
		WebElement formArea = driver.findElement(employeeForm);
		return formArea.findElement(entryField(fieldName)).getAttribute("value").trim();
	}

    /**************************************************************************************
     * Go back to the list of all employees
     **************************************************************************************/
	public void backToListingPage() {
		driver.findElement(backButton).click();
	}
}
