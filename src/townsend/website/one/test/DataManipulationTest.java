package townsend.website.one.test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import townsend.website.one.pages.CreatePage;
import townsend.website.one.pages.EditPage;
import townsend.website.one.pages.ListingPage;
import townsend.website.one.pages.LoginPage;

public class DataManipulationTest {
	
	WebDriver driver;
	String base_url;


    /**************************************************************************************
     * Opens the browser with the right capability before running the tests
     **************************************************************************************/
    @BeforeMethod(alwaysRun=true)
    @Parameters({"test_system","base_url"})
    public void setUp(String test_system,String base_url,ITestContext context) {
    	if (test_system.equals("Firefox")) {
            driver = new FirefoxDriver();
            driver.manage().window().maximize();
    	} else if (test_system.equals("Chrome")) {
    		driver = new ChromeDriver();
            driver.manage().window().maximize();
    	} else if (test_system.equals("Internet Explorer")) {
            DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
            ieCapabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
    		driver = new InternetExplorerDriver(ieCapabilities);
            driver.manage().window().maximize();
    	} else if(test_system.equals("Safari")) {
    		driver = new SafariDriver();
            driver.manage().window().maximize();
    	} else if (test_system.startsWith("Chrome_")) {
    		Map<String, String> mobileEmulation = new HashMap<String, String>();
    		mobileEmulation.put("deviceName", test_system.replace("Chrome_",""));

    		Map<String, Object> chromeOptions = new HashMap<String, Object>();
    		chromeOptions.put("mobileEmulation", mobileEmulation);
    		
    		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
    		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
    		
    		driver = new ChromeDriver(capabilities);
    		
    	} else {
    		System.out.println("Browser name " + test_system + " not found.");
    		assert(false);
    	}
		this.base_url = base_url;
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get(this.base_url);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("Luke", "Skywalker");
        loginPage.waitFor(1);
    }

    /**************************************************************************************
     * Close down the browser after all tests have run
     **************************************************************************************/
    @AfterMethod(alwaysRun=true)
    public void tearDown(ITestContext context) {
        driver.quit();
    }

    
    /*************************************************************************************
	 * Returns array of first name, last name, start date and email
     *************************************************************************************/
    @DataProvider(name="sampleEmployee")
    public Object[][] searchTerms() {
    	return CommonMethods.readCsv("sampleEmployee.csv");
    }

    /**************************************************************************************
     * First valid test case. Create then edit then delete.
     **************************************************************************************/
    @Test(groups = {"data"},dataProvider="sampleEmployee")
    public void basicTest(String in_firstName, String in_lastName, String in_startDate, String in_email) {
    	ListingPage listingPage = new ListingPage(driver);

    	// Create flow
    	listingPage.createNewEmployee();
    	CreatePage createPage = new CreatePage(driver);
    	createPage.fillInForm(in_firstName,in_lastName,in_startDate,in_email);
    	createPage.confirmCreate();

    	// Check that we are back in listing page then edit that same entry
    	listingPage.doesButtonExist("Create");
    	listingPage.editEmployee(in_firstName + " " + in_lastName);
    	EditPage editPage = new EditPage(driver);

    	// Verify that all the data is correct
    	assert(editPage.getCurrentValue("First name:").equals(in_firstName));
    	assert(editPage.getCurrentValue("Last name:").equals(in_lastName));
    	assert(editPage.getCurrentValue("Start date:").equals(in_startDate));
    	assert(editPage.getCurrentValue("Email:").equals(in_email));

    	// Then update all the data
    	editPage.fillInForm(in_firstName + "_2", in_lastName + "_2", in_startDate , "2" + in_email);
    	editPage.confirmUpdate();

    	// And then check the data
    	listingPage.doesButtonExist("Create");
    	listingPage.editEmployee(in_firstName + "_2 " + in_lastName + "_2");
    	assert(editPage.getCurrentValue("First name:").equals(in_firstName + "_2"));
    	assert(editPage.getCurrentValue("Last name:").equals(in_lastName + "_2"));
    	assert(editPage.getCurrentValue("Start date:").equals(in_startDate));
    	assert(editPage.getCurrentValue("Email:").equals("2" + in_email));
    	editPage.backToListingPage();

    	// Delete flow
    	listingPage.doesButtonExist("Create");
    	listingPage.deleteEmployee(in_firstName + "_2 " + in_lastName + "_2");
    	assert(listingPage.employeeExist(in_firstName + "_2 " + in_lastName + "_2"));

    }


    /**************************************************************************************
     * Check that all four fields are mandatory on create.
     * Also check date and email validation
     **************************************************************************************/
    @Test(groups = {"data"})
    public void createValidationTest() {
    	ListingPage listingPage = new ListingPage(driver);
    	listingPage.createNewEmployee();

    	// All fields should start out empty (and invalid)
    	CreatePage createPage = new CreatePage(driver);

    	// Check first name
    	assert(!createPage.isFieldValid("First name:"));
    	createPage.replaceSingleDetail("First name:", "abc");
    	assert(createPage.isFieldValid("First name:"));

    	// Check last name
    	assert(!createPage.isFieldValid("Last name:"));
    	createPage.replaceSingleDetail("Last name:", "abc");
    	assert(createPage.isFieldValid("Last name:"));

    	// Check start date
    	assert(!createPage.isFieldValid("Start date:"));
    	createPage.replaceSingleDetail("Start date:", "abc");
    	assert(!createPage.isFieldValid("Start date:"));
    	createPage.replaceSingleDetail("Start date:", "2014-05-25");
    	assert(createPage.isFieldValid("Start date:"));

    	// Check start date
    	assert(!createPage.isFieldValid("Email:"));
    	createPage.replaceSingleDetail("Email:", "abc");
    	assert(!createPage.isFieldValid("Email:"));
    	createPage.replaceSingleDetail("Email:", "abe@abc.com");
    	assert(createPage.isFieldValid("Email:"));
    }

    /**************************************************************************************
     * Check that all four fields are mandatory on edit.
     * Also check date and email validation
     **************************************************************************************/
    @Test(groups = {"data"})
    public void editValidationTest() {
    	ListingPage listingPage = new ListingPage(driver);
    	listingPage.createNewEmployee();

    	// Create new employee
    	CreatePage createPage = new CreatePage(driver);
    	createPage.fillInForm("abc", "def", "2016-05-25", "abc@def.com");
    	createPage.confirmCreate();

    	// Then go back and edit it. All fields should start out valid
    	listingPage.doesButtonExist("Create");
    	listingPage.editEmployee("abc def");
    	EditPage editPage = new EditPage(driver);
    	

    	// Check first name
    	assert(editPage.isFieldValid("First name:"));
    	editPage.replaceSingleDetail("First name:", "");
    	assert(!editPage.isFieldValid("First name:"));

    	// Check last name
    	assert(editPage.isFieldValid("Last name:"));
    	editPage.replaceSingleDetail("Last name:", "");
    	assert(!editPage.isFieldValid("Last name:"));

    	// Check start date
    	assert(editPage.isFieldValid("Start date:"));
    	editPage.replaceSingleDetail("Start date:", "");
    	assert(!editPage.isFieldValid("Start date:"));

    	// Check start date
    	assert(editPage.isFieldValid("Email:"));
    	editPage.replaceSingleDetail("Email:", "");
    	assert(!editPage.isFieldValid("Email:"));
    	
    	// Then delete it from this screen
    	editPage.deleteObject();
    }

}
