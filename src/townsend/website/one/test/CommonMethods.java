package townsend.website.one.test;

import java.io.FileReader;
import java.util.Vector;

import org.apache.commons.lang3.ArrayUtils;

import com.opencsv.CSVReader;

public class CommonMethods {

    /**************************************************************************************
     * This is the method for reading csv files
     * @filename - name of the file to be read, has to be in data folder
     * @col1Flag - any item matching this will be executed, normally Y
     **************************************************************************************/
    public static Object[][] readCsv(String filename) {

    	Vector<String[]> result = new Vector<String[]>(10,5);
    	CSVReader reader;
		try {
			reader = new CSVReader(new FileReader("data/" + filename));
	        String [] nextLine;
	        reader.readNext();  // We are discarding the column headers
	        while ((nextLine = reader.readNext()) != null) {
	        	result.add(nextLine);
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}

        // The number of times data is repeated, test will be executed the same no. of times
        return result.toArray(new String[result.size()][]);
    }

}
