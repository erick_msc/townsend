package townsend.website.one.test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import townsend.website.one.pages.ListingPage;
import townsend.website.one.pages.LoginPage;

public class CredentialsTest {
	
	WebDriver driver;
	String base_url;


    /**************************************************************************************
     * Opens the browser with the right capability before running the tests
     **************************************************************************************/
    @BeforeMethod(alwaysRun=true)
    @Parameters({"test_system","base_url"})
    public void setUp(String test_system,String base_url,ITestContext context) {
    	if (test_system.equals("Firefox")) {
            driver = new FirefoxDriver();
            driver.manage().window().maximize();
    	} else if (test_system.equals("Chrome")) {
    		driver = new ChromeDriver();
            driver.manage().window().maximize();
    	} else if (test_system.equals("Internet Explorer")) {
            DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
            ieCapabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
    		driver = new InternetExplorerDriver(ieCapabilities);
            driver.manage().window().maximize();
    	} else if(test_system.equals("Safari")) {
    		driver = new SafariDriver();
            driver.manage().window().maximize();
    	} else if (test_system.startsWith("Chrome_")) {
    		Map<String, String> mobileEmulation = new HashMap<String, String>();
    		mobileEmulation.put("deviceName", test_system.replace("Chrome_",""));

    		Map<String, Object> chromeOptions = new HashMap<String, Object>();
    		chromeOptions.put("mobileEmulation", mobileEmulation);
    		
    		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
    		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
    		
    		driver = new ChromeDriver(capabilities);
    		
    	} else {
    		System.out.println("Browser name " + test_system + " not found.");
    		assert(false);
    	}
		this.base_url = base_url;
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    /**************************************************************************************
     * Close down the browser after all tests have run
     **************************************************************************************/
    @AfterMethod(alwaysRun=true)
    public void tearDown(ITestContext context) {
        driver.quit();
    }
    
    /*************************************************************************************
	 * Returns a set of valid logins
     *************************************************************************************/
    @DataProvider(name="validLogins")
    public Object[][] searchTerms() {
    	return CommonMethods.readCsv("validLogins.csv");
    }
    
    /*************************************************************************************
	 * Test valid login credentials
     *************************************************************************************/
    @Test(groups = {"login"},dataProvider="validLogins")
	public void checkValidLogins(String username,String password) {
    	
    	// Get to the login page and login
    	driver.get(base_url);
    	LoginPage loginPage = new LoginPage(driver);
    	loginPage.login(username, password);

    	// Check that we are on the listing page by checking if Create button is there
    	ListingPage listingPage = new ListingPage(driver);
    	assert(listingPage.doesButtonExist("Create"));

    }

    /*************************************************************************************
	 * Returns a set of invalid logins and expected error messages
     *************************************************************************************/
    @DataProvider(name="invalidLogins")
    public Object[][] mainCategories() {
    	return CommonMethods.readCsv("invalidLogins.csv");
    }
    
    /*************************************************************************************
	 * Test error message for invalid login credentials
     *************************************************************************************/
    @Test(groups = {"login"},dataProvider="invalidLogins")
	public void checkInvalidLogins(String username,String password,String message) {
    	
    	// Get to the login page, check that error message is not displayed then login
    	driver.get(base_url);
    	LoginPage loginPage = new LoginPage(driver);
    	assert(!loginPage.isErrorVisible() || loginPage.getErrorMessage().trim().isEmpty());
    	loginPage.login(username, password);

    	// Check that we now see an error message
    	assert(loginPage.isErrorVisible());
    	assert(loginPage.getErrorMessage().trim().equals(message));

    }

}